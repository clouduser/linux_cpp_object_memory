#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include <typeinfo>

using namespace std;

class  Empty
{
	char d;
};

int main ()
{
	Empty ee;
	cout << "1empty_size:" << sizeof(ee) << endl;

	Empty *pE = new Empty();
	cout << "n0:" << typeid(*pE).name() <<  endl;

	delete pE;

	return 0;
}
