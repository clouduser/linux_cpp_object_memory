#include <iostream>
#include <stdio.h>
#include <stdint.h>
#include <typeinfo>

using namespace std;

class Base 
{
	public:
		Base():imember_(10) {}
		virtual void f()
		{
			cout << "base: f()" << endl;
		}
		virtual void g()
		{
			cout << "base: g()" << endl;
		}
		virtual void h()
		{
			cout << "base: h()" << endl;
		}
		void nouse()
		{
			cout << "base: nouse()" << endl;
		}

	public:
		int imember_;
};

class Derived : public Base 
{
	public:
		Derived():imember_(100) {}
		virtual void f2()
		{
			cout << "derived: f2()" << endl;
		}
		virtual void g()
		{
			cout << "derived: g()" << endl;
		}
		virtual void h2()
		{
			cout << "derived: h2()" << endl;
		}

	public:
		int imember_;

}; 

int main ()
{
	Base bb;
	Derived dd;
	cout << "base_size:" << sizeof(bb) << endl;
	cout << "derived_size:" << sizeof(dd) << endl;

	typedef void (*Func)();
	Func pf;

	Base *pB = new Derived();
	int *plink =  (int*)*((int*)pB);
	cout << "vptr_header:" << endl;
	cout << "n0:" << *(plink+0) <<  endl;
	pf = (Func)*plink;
	pf();

	cout << "n1:" << *(plink+1) <<  endl;//NULL placeholder
	pf = (Func)*(plink+2);
	pf();

	cout << "n2:" << *(plink+3) << endl;//NULL placeholder
	pf = (Func)*(plink+4);
	pf();

	cout << "n3:" << *(plink+5) << endl;//NULL placeholder
	pf = (Func)*(plink+6);
	pf();

	cout << "n4:" << *(plink+7) << endl;//NULL placeholder
	pf = (Func)*(plink+8);
	pf();

	plink =  (int*)pB;
	cout << "member_header:" << endl;
	cout << "m1:" << *(plink+1) << endl;//NULL placeholder
	cout << "m2:" << *(plink+2) << endl;
	cout << "m3:" << *(plink+3) << endl;

	delete pB;

	return 0;
}
