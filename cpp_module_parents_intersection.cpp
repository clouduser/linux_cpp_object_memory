#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdint.h>
#include <typeinfo>

using namespace std;

class Base1 
{
	public:
		Base1():imember_(10) {}
		virtual void intersection()
		{
			cout << "base1: intersection()" << endl;
		}

	public:
		int imember_;
};

class Base2 
{
	public:
		Base2():imember_(11) {}
		virtual void m()
		{
			cout << "base2: m()" << endl;
		}
		virtual void intersection()
		{
			cout << "base2: intersection()" << endl;
		}

	public:
		int imember_;
};

class Derived : public Base1, Base2
{
	public:
		Derived():imember_(100) {}
		virtual void m()
		{
			cout << "derived: m()" << endl;
		}
		virtual void intersection()
		{
			cout << "derived: intersection()" << endl;
		}

	public:
		int imember_;

}; 

int main ()
{
	Base1 b1;
	Base2 b2;
	Derived dd;
	cout << "base1_size:" << sizeof(b1) << endl;
	cout << "base2_size:" << sizeof(b2) << endl;
	cout << "derived_size:" << sizeof(dd) << endl;
	cout << "-------------------" << endl;

	typedef void (*Func)();
	Func pf;

	Derived *pD = new Derived();
	int *plink =  (int*)*((int*)pD);

	cout << "vptr_header_base1:" << endl;
	{
		cout << "n0:" << *(plink+0) <<  endl;
		pf = (Func)*plink;
		pf();

		cout << "n1:" << *(plink+1) <<  endl;//NULL placeholder
		pf = (Func)*(plink+2);
		pf();
		cout << "n2:" << *(plink+3) << endl;//NULL placeholder
	}

	plink =  (int*)pD;
	cout << "member_header:" << endl;
	cout << "m1:" << *(plink+1) << endl;//NULL placeholder
	cout << "m2:" << *(plink+2) << endl;
	cout << "m3:" << *(plink+3) << endl;

	plink += 4;//_vptr_base2
	plink =  (int*)*((int*)plink);
	cout << "vptr_header_base2:" << endl;
	{
		cout << "n0:" << *(plink+0) <<  endl;
		pf = (Func)*plink;
		pf();

		cout << "n1:" << *(plink+1) <<  endl;//NULL placeholder
		pf = (Func)*(plink+2);
		pf();

		cout << "n2:" << *(plink+3) << endl;//NULL placeholder
	}

	plink =  (int*)pD;
	cout << "member_header:" << endl;
	cout << "m1:" << *(plink+5) << endl;//NULL placeholder
	cout << "m2:" << *(plink+6) << endl;
	cout << "m3:" << *(plink+7) << endl;
	cout << "m4:" << *(plink+8) << endl;

	delete pD;

	return 0;
}
