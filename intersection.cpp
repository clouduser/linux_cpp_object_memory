#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdint.h>
#include <typeinfo>

using namespace std;

class Base1 
{
	public:
		Base1():imember_(10) {}
		virtual void intersection()
		{
			cout << "base1: intersection()" << endl;
		}

	public:
		int imember_;
};

class Base2 
{
	public:
		Base2():imember_(11) {}
		virtual void m()
		{
			cout << "base2: m()" << endl;
		}
		virtual void intersection()
		{
			cout << "base2: intersection()" << endl;
		}

	public:
		int imember_;
};

class Derived : public Base1, Base2
{
	public:
		Derived():imember_(100) {}
		virtual void m()
		{
			cout << "derived: m()" << endl;
		}
		virtual void intersection()
		{
			cout << "derived: intersection()" << endl;
		}

	public:
		int imember_;

}; 

int main ()
{
	typedef void (*Func)();
	Func pf;

	Derived *pD = new Derived();
	((Base1*)pD)->intersection();//Derived vptr === Base1 header
	((Base2*)pD)->intersection();//!!: pD -> Base2*    need temp adjust chunk

	delete pD;

	return 0;
}
